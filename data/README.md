## Source Data

The source data was generated on 2019-05-28 with these commands:

```
git clone https://git.drupalcode.org/project/drupal.git
git log --date=short --shortstat --pretty=format:"%h%x09%an%x09%ad%x09%sEOL" | perl -p -e 's/EOL\n/\t/g' > commits.tsv
sed -i '/^$/d' commits.tsv
```
