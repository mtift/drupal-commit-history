The report can be generated from the command line with the following command:

```
R -e "rmarkdown::render('drupal-commit-history.Rmd')"
```
